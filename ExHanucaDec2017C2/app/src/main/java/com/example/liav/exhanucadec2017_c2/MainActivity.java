package com.example.liav.exhanucadec2017_c2;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.TextView;
import java.util.Random;

public class MainActivity extends Activity {

    private TextView view1;
    private EditText edit1,edit2;
    private Button buttonAdd,buttonSub,buttonMul,buttonDiv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edit1 = (EditText) findViewById(R.id.editText);
        edit2 = (EditText) findViewById(R.id.editText2);
        view1 = (TextView) findViewById(R.id.textView3);
        addListenerOnButton();
    }

    public void addListenerOnButton()
    {

        buttonAdd = (Button)findViewById(R.id.buttonadd);

        buttonAdd.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String text = "";
                String text1 = edit1.getText().toString();
                String text2 = edit1.getText().toString();
                double n=0;
                try {
                    int num1 = Integer.parseInt(text1);
                    int num2 = Integer.parseInt(text2);
                    n = num1 + num2;
                    view1.setText("" + n);
                }
                catch(NumberFormatException e)
                {
                    n = 0;
                    String text_toast = "Wrong input";
                    view1.setText("Error");
                }


            }

        });
        buttonSub = (Button)findViewById(R.id.buttonsub);

        buttonSub.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String text = "";
                String text1 = edit1.getText().toString();
                String text2 = edit1.getText().toString();
                double n=0;
                try {
                    int num1 = Integer.parseInt(text1);
                    int num2 = Integer.parseInt(text2);
                    n = num1 - num2;
                    view1.setText("" + n);

                }
                catch(NumberFormatException e)
                {
                    n = 0;
                    String text_toast = "Wrong input";
                    view1.setText("Error");
                }


            }

        });

        buttonMul = (Button)findViewById(R.id.button2);

        buttonMul.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String text = "";
                String text1 = edit1.getText().toString();
                String text2 = edit1.getText().toString();
                double n=0;
                try {
                    int num1 = Integer.parseInt(text1);
                    int num2 = Integer.parseInt(text2);
                    n = num1 * num2;
                    view1.setText("" + n);
                }
                catch(NumberFormatException e)
                {
                    n = 0;
                    String text_toast = "Wrong input";
                    view1.setText("Error");
                }


            }

        });

        buttonDiv = (Button)findViewById(R.id.button);

        buttonDiv.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String text = "";
                String text1 = edit1.getText().toString();
                String text2 = edit1.getText().toString();
                double n=0;
                try {
                    int num1 = Integer.parseInt(text1);
                    int num2 = Integer.parseInt(text2);
                    if(num2 == 0) {
                        n = 0;
                    }
                    else {
                        n = num1 / num2;
                    }
                    view1.setText("" + n);
                }
                catch(NumberFormatException e)
                {
                    n = 0;
                    String text_toast = "Wrong input";
                    view1.setText("Error");
                }


            }

        });
    }
}