package com.example.liav.xo_game;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import android.widget.Button;
import android.widget.TextView;
import android.view.*;
import android.media.MediaPlayer;
import java.util.Random;

public class MainActivity extends Activity {

    private Button[][] btns;
    private Button start;
    private TextView tv;
    private int[][] places;
    private Vibrator v;
    private Context context;

    private boolean has_to_newgame=false;
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btns = new Button[3][3];
        btns[0][0] = (Button) findViewById(R.id.button);
        btns[0][1] = (Button) findViewById(R.id.button2);
        btns[0][2] = (Button) findViewById(R.id.button3);
        btns[1][0] = (Button) findViewById(R.id.button4);
        btns[1][1] = (Button) findViewById(R.id.button5);
        btns[1][2] = (Button) findViewById(R.id.button6);
        btns[2][0] = (Button) findViewById(R.id.button7);
        btns[2][1] = (Button) findViewById(R.id.button8);
        btns[2][2] = (Button) findViewById(R.id.button9);
        start = (Button) findViewById(R.id.button_start);
        places = new int[3][3];
        tv = (TextView) findViewById(R.id.textView);

        /*for(int i=0;i<3;i++) {

            for (int j = 0; j < 3; j++) {
                places[i][j] = 0;
            }
        }*/

        for(int i=0;i<3;i++) {

            for(int j=0;j<3;j++)
            {
                final int b = i;
                final int c = j;
                btns[i][j].setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v)
                    {
                        Play(b,c);
                    }
                });
            }
        }
        start.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
                Restart();
            }
        });



    }

    protected void Play(int i,int j)
    {
        CheckWin();

        if(has_to_newgame == false) {
            if (places[i][j] == 0) {
                places[i][j] = 1;
                btns[i][j].setText("X");
                //Flip(v);
                CheckWin();
                //computer plays....
                Random r = new Random();
                if (CheckSlotsAvailable() > 1) {
                    boolean computer_try = true;
                    while (computer_try) {
                        int i1 = (r.nextInt(3));
                        int i2 = (r.nextInt(3));
                        if (AvailabePlace(i1, i2)) {
                            places[i1][i2] = 2;
                            btns[i1][i2].setText("O");
                            computer_try = false;
                        }
                    }
                    CheckWin();
                }

            }
        }
        if(CheckSlotsAvailable() == 0)
        {
            final MediaPlayer mp = MediaPlayer.create(this, R.raw.beep37);
            mp.start();
            has_to_newgame = true;
            tv.setText("O & X did not win!");

        }
    }
    protected int CheckForMatchRow()
    {
        for(int i=0;i<3;i++)
        {
            if(places[i][0] == places[i][1] && places[i][1] == places[i][2] && places[i][0] != 0)
                return i;
        }
        return -1;
    }
    protected int CheckForMatchCol()
    {
        for(int i=0;i<3;i++)
        {
            if(places[0][i] == places[1][i] && places[1][i] == places[2][i] && places[0][i] != 0)
                return i;
        }
        return -1;
    }
    protected boolean CheckForMatchDiagonal1()
    {
        if(places[0][0] == places[1][1] && places[1][1] == places[2][2] && places[1][1] != 0)
            return true;
        else
            return false;
    }
    protected boolean CheckForMatchDiagonal2()
    {
        if(places[2][0] == places[1][1] && places[1][1] == places[0][2] && places[1][1] != 0)
            return true;
        else
            return false;
    }
    protected void CheckWin()
    {

        if(CheckForMatchCol() != (-1))
        {
            if(places[0][CheckForMatchCol()] == 1)
            {
                final MediaPlayer mp = MediaPlayer.create(this, R.raw.beep30);
                mp.start();
                tv.setText("X won");
            }
            else
            {
                final MediaPlayer mp = MediaPlayer.create(this, R.raw.beep34);
                mp.start();
                tv.setText("O won");
            }
            has_to_newgame = true;
        }
        else if(CheckForMatchRow() != (-1))
        {
            if(places[CheckForMatchRow()][0] == 1)
            {
                final MediaPlayer mp = MediaPlayer.create(this, R.raw.beep30);
                mp.start();
                tv.setText("X won");
            }
            else if(places[0][0] == 2)
            {
                final MediaPlayer mp = MediaPlayer.create(this, R.raw.beep34);
                mp.start();
                tv.setText("O won");
            }
            has_to_newgame = true;
        }
        else if(CheckForMatchDiagonal1())
        {
            if(places[0][0] == 1)
            {
                final MediaPlayer mp = MediaPlayer.create(this, R.raw.beep30);
                mp.start();
                tv.setText("X won");
            }
            else if(places[0][0] == 2)
            {
                final MediaPlayer mp = MediaPlayer.create(this, R.raw.beep34);
                mp.start();
                tv.setText("O won");
            }
            has_to_newgame = true;
        }
        else if(CheckForMatchDiagonal2())
        {
            if(places[0][2] == 1)
            {
                final MediaPlayer mp = MediaPlayer.create(this, R.raw.beep30);
                mp.start();
                tv.setText("X won");
            }
            else if(places[0][2] == 2)
            {
                final MediaPlayer mp = MediaPlayer.create(this, R.raw.beep34);
                mp.start();
                tv.setText("O won");
            }
            has_to_newgame = true;
        }
        if(has_to_newgame == true)
        {
            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            //v.vibrate(100);
            v.vibrate(100);
        }

    }

    protected void Restart()
    {
        for(int i=0;i<3;i++) {

            for (int j = 0; j < 3; j++) {
                places[i][j] = 0;
            }
        }
        for(int i=0;i<3;i++) {

            for (int j = 0; j < 3; j++) {
                btns[i][j].setText("_");
            }
        }
        has_to_newgame = false;
        tv.setText("New game.");
    }
    protected int CheckSlotsAvailable()
    {
        int count = 0;
        for(int i=0; i < 3; i++)
        {
            for(int j=0;j < 3; j++)
            {
                if(places[i][j] == 0)
                    count++;
            }
        }
        return count;
    }
    protected boolean AvailabePlace(int i,int j) {
        if (places[i][j] != 2 && places[i][j] != 1) {
            return true;

        } else {
            return false;

        }
    }


}
