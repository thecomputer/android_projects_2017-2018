package com.example.liav.ex2nov24_2017;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import java.util.Random;

public class MainActivity extends Activity {

    private TextView view1;
    private Button buttonSum,buttonAdd,buttonSub;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        view1 = (TextView) findViewById(R.id.view1);
        view1.setText("");
        addListenerOnButton();
    }

    public void addListenerOnButton()
    {

        buttonSum = (Button)findViewById(R.id.button1);

        buttonSum.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                view1 = (TextView) findViewById(R.id.view1);
                Random rand = new Random();
                int a;
                a = rand.nextInt(89);
                a = a + 10;
                view1.setText(Integer.toString(a));
            }

        });
        buttonAdd = (Button)findViewById(R.id.buttonadd);

        buttonAdd.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String value = view1.getText().toString();
                int a = Integer.parseInt(value);
                a = a + 1;
                view1.setText(Integer.toString(a));
            }

        });
        buttonSub = (Button)findViewById(R.id.buttonsub);

        buttonSub.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String value = view1.getText().toString();
                int a = Integer.parseInt(value);
                a = a - 1;
                view1.setText(Integer.toString(a));
            }

        });
    }
}
