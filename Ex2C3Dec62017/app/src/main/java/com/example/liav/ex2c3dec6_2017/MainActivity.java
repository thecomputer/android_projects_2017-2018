package com.example.liav.ex2c3dec6_2017;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.EditText;
import java.util.Random;

public class MainActivity extends Activity {

    private TextView title;
    private EditText edit1,edit2,edit3;
    private Button buttonSum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addListenerOnButton();
    }

    public void addListenerOnButton()
    {
        edit1 = (EditText)findViewById(R.id.edit1);
        edit2 = (EditText)findViewById(R.id.edit2);
        edit3 = (EditText)findViewById(R.id.edit3);
        buttonSum = (Button)findViewById(R.id.button1);
        title = (TextView) findViewById(R.id.avgCap);
        buttonSum.setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View v) {
                String value1 = edit1.getText().toString();
                String value2 = edit2.getText().toString();
                String value3 = edit3.getText().toString();
                int a = Integer.parseInt(value1);
                int b = Integer.parseInt(value2);
                int c = Integer.parseInt(value3);
                int d = ((a/b)*(c*(1/100)));

                title.setText(""+d);
            }


        });

    }
}