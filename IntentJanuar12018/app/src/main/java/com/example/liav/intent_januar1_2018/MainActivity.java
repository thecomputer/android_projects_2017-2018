package com.example.liav.intent_januar1_2018;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.EditText;
import android.view.*;

public class MainActivity extends Activity implements View.OnClickListener {

    private Button btn1;
    private EditText edit1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        btn1 = (Button) findViewById(R.id.btn1);
        edit1 = (EditText) findViewById(R.id.et);

        btn1.setOnClickListener((View.OnClickListener) this);

    }

    @Override
    public void onClick(View v) {

    }
}
