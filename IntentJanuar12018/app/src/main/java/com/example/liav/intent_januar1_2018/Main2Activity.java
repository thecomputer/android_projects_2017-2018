package com.example.liav.intent_januar1_2018;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;
import android.content.Intent;
public class Main2Activity extends Activity {

    private TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        tv = (TextView) findViewById(R.id.tv);
        Intent i = getIntent();
        String firstname = i.getExtras().getString("fname");

        tv.setText(firstname);

    }
}
