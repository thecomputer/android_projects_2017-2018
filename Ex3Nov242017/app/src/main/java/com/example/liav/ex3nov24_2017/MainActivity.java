package com.example.liav.ex3nov24_2017;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import java.util.Random;

public class MainActivity extends Activity {

    private TextView view1,view2,view3;
    private Button buttonSum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        view1 = (TextView) findViewById(R.id.view1);
        view2 = (TextView) findViewById(R.id.view2);
        view1.setText("");
        view2.setText("");
        addListenerOnButton();
    }

    public void addListenerOnButton()
    {

        buttonSum = (Button)findViewById(R.id.button1);

        buttonSum.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                view1 = (TextView) findViewById(R.id.view1);
                view2 = (TextView) findViewById(R.id.view2);
                Random rand = new Random();
                int a;
                a = rand.nextInt(89);
                a = a + 10;
                int b;
                b = rand.nextInt(89);
                b = b + 10;
                view1.setText(Integer.toString(a));
                view2.setText(Integer.toString(b));
                if(a>b) {
                    view1.setTextSize(25);
                    view1.setTextColor(Color.parseColor("#ff99cc00"));
                    view2.setTextSize(15);
                    view2.setTextColor(Color.parseColor("#ffff4444"));
                }
                else
                {
                    view2.setTextSize(25);
                    view2.setTextColor(Color.parseColor("#ff99cc00"));
                    view1.setTextSize(15);
                    view1.setTextColor(Color.parseColor("#ffff4444"));
                }

            }

        });

    }
}

