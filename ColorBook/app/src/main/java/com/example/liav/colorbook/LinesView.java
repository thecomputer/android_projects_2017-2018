package com.example.liav.colorbook;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;
import android.view.MotionEvent;
import android.widget.LinearLayout;

/**
 * Created by liav on 1/26/18.
 */

public class LinesView extends View {

    private int mShapeColor = Color.BLACK;

            /*     enum could be nicer but takes more system resources: */

    private final int LINE = 0;

    private final int CIRCLE = 1;

    private int mShapeType = LINE;

    private final Lines lines;

    float mStartLineX;

    float mStartLineY;

    float mStopLineX;

    float mStopLineY;

    Paint paint;

    public LinesView(Context context, Lines lines) {

        super(context);

        this.lines = lines;

        setMinimumWidth(LinearLayout.LayoutParams.MATCH_PARENT);

        setMinimumHeight(LinearLayout.LayoutParams.MATCH_PARENT);

        paint = new Paint(Paint.ANTI_ALIAS_FLAG);

        paint.setStyle(Paint.Style.STROKE);

        paint.setStrokeWidth(4);

    }

    @Override

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        setMeasuredDimension(

                getSuggestedMinimumWidth(),

                getSuggestedMinimumHeight());

    }

    @Override
    protected void onDraw(Canvas canvas) {

        canvas.drawColor(Color.WHITE);

        mCurrentlyTouchedShapeDraw(canvas, paint);

        mAllShapesDraw(canvas, paint);

    }

    @Override

    public boolean onTouchEvent(MotionEvent event) {

        int action = event.getAction();

        if (action == MotionEvent.ACTION_DOWN) {

            mStartLineX = event.getX();

            mStartLineY = event.getY();

        } else if (action == MotionEvent.ACTION_MOVE) {

            mStopLineX = event.getX();

            mStopLineY = event.getY();

            invalidate();

        } else if (action == MotionEvent.ACTION_UP) {

            lines.addLine(mStartLineX, mStartLineY, event.getX(), event.getY(), mShapeColorGet(), mShapeTypeGet());

            invalidate();

        }

        return true;

    }

    public void lastShapeDelete()

    {

        currentDrawCancel();

        lines.lastLineRemove();

        invalidate();

    }


    public void viewScreenClean() {

        currentDrawCancel();

        lines.clearLines();

        invalidate();

    }


    public void currentDrawCancel()

    {

        mStartLineX = 0;

        mStartLineY = 0;

        mStopLineX = 0;

        mStopLineY = 0;

    }


    private void mCurrentlyTouchedShapeDraw(Canvas canvas, Paint paint) {

        paint.setColor(mShapeColorGet());

        switch (mShapeTypeGet()) {

            case LINE:

                canvas.drawLine(mStartLineX, mStartLineY, mStopLineX, mStopLineY, paint);

                break;

            case CIRCLE:

                float startX = mStartLineX;

                float startY = mStartLineY;

                float stopX = mStopLineX;

                float stopY = mStopLineY;

                float radius = (float) (Math.sqrt(Math.pow(stopX - startX, 2) + Math.pow(stopY - startY, 2)));

                canvas.drawCircle(startX, startY, radius, paint);

                break;

        }

    }

    private void mAllShapesDraw(Canvas canvas, Paint paint) {

        for (Line line : lines.getLines()) {

            paint.setColor(line.getColor());

            switch (line.shapeGet()) {

                case LINE:

                    canvas.drawLine(line.getStartX(), line.getStartY(), line.getStopX(), line.getStopY(), paint);

                    break;

                case CIRCLE:

                    float stopX = line.getStopX();

                    float stopY = line.getStopY();

                    float startX = line.getStartX();

                    float startY = line.getStartY();

                    float radius = (float) (Math.sqrt(Math.pow(stopX - startX, 2) + Math.pow(stopY - startY, 2)));

                    canvas.drawCircle(startX, startY, radius, paint);

                    break;

            }

        }

    }

    public void mShapeColorSet(int color) {

        mShapeColor = color;

    }

    public int mShapeColorGet() {

        return (mShapeColor);

    }

    public void mShapeSetLine()

    {

        mShapeType = LINE;

    }

    public void mShapeSetCircle()

    {

        mShapeType = CIRCLE;

    }

    private int mShapeTypeGet()

    {

        return (mShapeType);

    }

}
