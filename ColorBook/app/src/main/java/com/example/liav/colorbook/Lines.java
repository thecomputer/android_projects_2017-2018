package com.example.liav.colorbook;

import java.util.*;

/**
 * Created by liav on 1/26/18.
 */

public  class Lines     {

    private  final LinkedList<Line> lines = new LinkedList<Line>();

    public Line     getLastLine() {

        return (lines.size()     <= 0) ? null : lines.getLast();

    }

    public     void lastLineRemove()     {

        if(lines.size()     > 0){

            lines.removeLast();

        }

        notifyListener();

    }
    public interface LinesChangeListener     {

        void onLinesChange(Lines lines);

    }

    public List<Line>     getLines() {

        return lines;

    }

    public     void addLine(float     startX, float startY, float stopX, float stopY, int color, int     width) {

        lines.add(new     Line( startX,  startY,  stopX,  stopY,  color,      width));

        notifyListener();

    }

    public     void clearLines()     {

        lines.clear();

        notifyListener();

    }



    private LinesChangeListener linesChangeListener;

    public     void setLinesChangeListener(LinesChangeListener     listener) {

        linesChangeListener     = listener;

    }

    private     void notifyListener()     {

        if     (null != linesChangeListener) {

            linesChangeListener.onLinesChange(this);

        }

    }

}