package com.example.liav.colorbook;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.view.View;
import android.widget.LinearLayout;


//import com.example.liav.colorbook.Line;
//import com.example.liav.colorbook.LinesView;

import java.util.LinkedList;

//public class MainActivity extends Activity {

  //  @Override
    //protected void onCreate(Bundle savedInstanceState) {
    //   super.onCreate(savedInstanceState);
      //  setContentView(R.layout.activity_main);
    //}
///}

public     class MainActivity     extends Activity {

    /**     Dot diameter */

    final     int LINE =     0;

    final     int CIRCLE =     1;

    final     int shape[] = {CIRCLE,     LINE,};

    int mColorIndex =     0;

    int mShapeIndex =     0;

    Button mButtonSetColor;

    Button mButtonSetShape;

    Button mButtonDelLastShape;

    EditText editTextstartX;

    EditText editTextstartY;

    EditText editTextstopX;

    EditText editTextstopY;

    final Lines lines =     new Lines();

    /**     The application view */

    LinesView linesView;



    /**     Called when the activity is first created. */

    @Override public     void onCreate(Bundle     state) {

        super.onCreate(state);

        mLinesChangeListenerSet();

        mViewSetup();

        mButtonsSet();

        mEditTextSet();
    }
        /*public class DrawControlActivity extends Activity {

            /**
             * Dot diameter
             */
            /*
            final int LINE = 0;

            final int CIRCLE = 1;

            final int shape[] = {CIRCLE, LINE,};

            int mColorIndex = 0;

            int mShapeIndex = 0;

            Button mButtonSetColor;

            Button mButtonSetShape;

            Button mButtonDelLastShape;

            EditText editTextstartX;

            EditText editTextstartY;

            EditText editTextstopX;

            EditText editTextstopY;

            final Lines lines = new Lines();
            */
            /**
             * The application view
             */

            //LinesView linesView;


            /**
             * Called when the activity is first created.
             */

            //@Override
            //public void onCreate(Bundle state) {

              //  super.onCreate(state);

                //mLinesChangeListenerSet();

                //mViewSetup();

            //}

            private void mViewSetup() {

                setContentView(R.layout.activity_main);

                linesView = new LinesView(this, lines);

                ((LinearLayout) findViewById(R.id.root4)).addView(linesView);
                //View view_instance = linesView;
                //linesView.getLayoutParams().width = LinearLayout.LayoutParams.MATCH_PARENT;
                //linesView.getLayoutParams().height = LinearLayout.LayoutParams.MATCH_PARENT;
                //linesView.requestLayout();
                //linesView.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT,ViewGroup.LayoutParams.FILL_PARENT);)
                //LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,ViewGroup.LayoutParams.FILL_PARENT);
                //view_instance.setLayoutParams(lp);

                //view_instance.getLayoutParams().width = LinearLayout.LayoutParams.MATCH_PARENT;

            }

            private void mLinesChangeListenerSet() {

                lines.setLinesChangeListener(new Lines.LinesChangeListener() {

                    public void onLinesChange(Lines lines) {

                        Line localLine = lines.getLastLine();

                        editTextstartX.setText((null == localLine) ? "" : String.valueOf(localLine.getStartX()));

                        editTextstartY.setText((null == localLine) ? "" : String.valueOf(localLine.getStartY()));

                        editTextstopX.setText((null == localLine) ? "" : String.valueOf(localLine.getStopX()));

                        editTextstopY.setText((null == localLine) ? "" : String.valueOf(localLine.getStopY()));

                    }
                });

            }

            private void mButtonsSet() {

                final int mLineColor[] = {Color.GREEN, Color.RED, Color.BLUE, Color.BLACK};

                mButtonSetColor = (Button) findViewById(R.id.btn1);

                mButtonSetColor.setOnClickListener(

                        new Button.OnClickListener() {
                            public void onClick(View v) {

                                setLinesColor(mLineColor[(mColorIndex) % mLineColor.length]);

                                mButtonSetColor.setTextColor(mLineColor[(mColorIndex++) % mLineColor.length]);

                            }
                        });

                ((Button) findViewById(R.id.btn2)).setOnClickListener(

                        new Button.OnClickListener() {

                            public void onClick(View v) {

                                viewScreenClean();

                            }
                        });

                mButtonSetShape = (Button) findViewById(R.id.btn3);

                mButtonSetShape.setOnClickListener(

                        new Button.OnClickListener() {

                            public void onClick(View v) {

                                switch (shape[(mShapeIndex++) % shape.length]) {

                                    case LINE:

                                        mShapeSet(LINE);

                                        mButtonSetShape.setText("Line");

                                        break;

                                    case CIRCLE:

                                        mShapeSet(CIRCLE);

                                        mButtonSetShape.setText("Circle");

                                        break;

                                }

                            }
                        });

                mButtonDelLastShape = (Button) findViewById(R.id.btn4);

                mButtonDelLastShape.setOnClickListener(new Button.OnClickListener() {

                    public void onClick(View v) {

                        lastShapeDelete();

                    }
                });

            }

            private void mEditTextSet() {

                editTextstartX = (EditText) findViewById(R.id.txt1);

                editTextstartY = (EditText) findViewById(R.id.txt2);

                editTextstopX = (EditText) findViewById(R.id.txt3);

                editTextstopY = (EditText) findViewById(R.id.txt4);

            }

        /*     Callbacks' Service Methods */

            private void setLinesColor(int color) {

                linesView.mShapeColorSet(color);

            }

            private void viewScreenClean() {

                linesView.viewScreenClean();

            }

            private void lastShapeDelete() {

                linesView.lastShapeDelete();

            }

            private void mShapeSet(int shape) {

                switch (shape) {

                    case LINE:

                        linesView.mShapeSetLine();

                        break;

                    case CIRCLE:

                        linesView.mShapeSetCircle();

                        break;

                }

            }

        }
















    