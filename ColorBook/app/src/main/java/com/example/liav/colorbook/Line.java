package com.example.liav.colorbook;

/**
 * Created by liav on 1/26/18.
 */

public class Line     {

    private     final float mStartX;

    private     final float mStartY;

    private     final float mStopX;

    private     final float mStopY;

    private     final int mColor;

    private     final int mShape;

    public     Line(float startX, float startY, float stopX, float stopY, int     color, int shape) {

        mStartX     = startX;

        mStartY     = startY;

        mStopX     = stopX;

        mStopY     = stopY;

        mColor     =  color;

        mShape     = shape;

    }

    public     float getStartX() { return mStartX;     }

    public     float getStartY() { return mStartY;     }

    public     float getStopX() { return mStopX;     }

    public     float getStopY() { return mStopY;     }

    public     int getColor() { return mColor;     }

    public     int shapeGet() { return mShape;     }

}