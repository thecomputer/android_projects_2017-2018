package com.example.liav.xo_game_jan5_finaltest;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import android.widget.Button;
import android.widget.TextView;
import android.view.*;
import android.media.MediaPlayer;

import java.security.PrivateKey;
import java.util.Random;
import android.widget.RadioButton;

public class MainActivity extends Activity {

    private Button[][] btns;
    private Button start,start2;
    private TextView tv;
    private RadioButton rb1,rb2;
    //
    public static final int amount_of_games = 5;

    public int amount_of_current_games;
    private int amount_of_moves_player1;
    private int amount_of_moves_player2;

    private int wins_of_player1;
    private int wins_of_player2;

    private boolean pvp = false;
    private int player_playing;
    ///
    private int[][] places;
    private Vibrator v;
    private Context context;

    private boolean has_to_newgame=false;
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        amount_of_current_games=amount_of_games;

        btns = new Button[3][3];
        btns[0][0] = (Button) findViewById(R.id.button);
        btns[0][1] = (Button) findViewById(R.id.button2);
        btns[0][2] = (Button) findViewById(R.id.button3);
        btns[1][0] = (Button) findViewById(R.id.button4);
        btns[1][1] = (Button) findViewById(R.id.button5);
        btns[1][2] = (Button) findViewById(R.id.button6);
        btns[2][0] = (Button) findViewById(R.id.button7);
        btns[2][1] = (Button) findViewById(R.id.button8);
        btns[2][2] = (Button) findViewById(R.id.button9);
        start = (Button) findViewById(R.id.button_start);
        start2 = (Button) findViewById(R.id.button_start2);
        places = new int[3][3];

        player_playing = 1;
        amount_of_moves_player1 = 0;
        amount_of_moves_player2 = 0;

        wins_of_player1 = 0;
        wins_of_player2 = 0;

        rb1 = (RadioButton)findViewById(R.id.radioButton);
        rb2 = (RadioButton) findViewById(R.id.radioButton2);
        tv = (TextView) findViewById(R.id.textView);

        /*for(int i=0;i<3;i++) {

            for (int j = 0; j < 3; j++) {
                places[i][j] = 0;
            }
        }*/

        for(int i=0;i<3;i++) {

            for(int j=0;j<3;j++)
            {
                final int b = i;
                final int c = j;
                btns[i][j].setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v)
                    {
                        Play(b,c);
                    }
                });
            }
        }
        start.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
                Restart();
            }
        });
        start2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
                amount_of_current_games=amount_of_games;
                Restart();
            }
        });



    }

    public void onRadioButtonClick( View v)
    {
        switch (v.getId())
        {
            case R.id.radioButton:
                pvp = false;
                rb2.setChecked(false);
                break;
            case R.id.radioButton2:
                pvp = true;
                rb1.setChecked(false);
                break;
        }
    }

    protected void Play(int i,int j) {
        CheckWin();
        if (!ExistMaraton()) {
            if (wins_of_player1 > wins_of_player2)
                tv.setText("X won 0 in maraton! - End is end...(of maraton)");
            else {
                tv.setText("0 won X in maraton! - End is end...(of maraton)");
            }
        }
        else {
            if (player_playing == 1 && pvp) {
                if (!has_to_newgame) {
                    if (places[i][j] == 0) {
                        places[i][j] = 1;
                        btns[i][j].setText("X");
                        //Flip(v);
                        amount_of_moves_player1++;
                        CheckWin();

                        player_playing = 2;

                    }
                }
            }
            else if(player_playing == 2 && pvp)
            {
                if (!has_to_newgame) {
                    if (places[i][j] == 0) {
                        places[i][j] = 2;
                        btns[i][j].setText("0");
                        //Flip(v);
                        amount_of_moves_player2++;
                        CheckWin();

                        player_playing = 1;

                    }
                }
            }
            else if (!pvp) {
                if (!has_to_newgame) {
                    if (places[i][j] == 0) {
                        places[i][j] = 1;
                        btns[i][j].setText("X");
                        //Flip(v);
                        amount_of_moves_player1++;
                        CheckWin();
                        //computer plays....
                        Random r = new Random();
                        if (CheckSlotsAvailable() >= 1) {
                            boolean computer_try = true;
                            while (computer_try) {
                                int i1 = (r.nextInt(3));
                                int i2 = (r.nextInt(3));
                                if (AvailabePlace(i1, i2)) {
                                    places[i1][i2] = 2;
                                    btns[i1][i2].setText("O");
                                    computer_try = false;
                                }

                            }
                            amount_of_moves_player2++;
                            CheckWin();
                        }
                    }
                }

            }
            CheckWin();

            if (CheckSlotsAvailable() == 0 && has_to_newgame != true) {
                has_to_newgame = true;
                tv.setText("O & X לא תותחים...!");

            }
        }
        }
        protected boolean ExistMaraton()
        {
            if(amount_of_current_games > 0)
                return true;
            else
                return false;
        }
    protected int CheckForMatchRow()
    {
        for(int i=0;i<3;i++)
        {
            if(places[i][0] == places[i][1] && places[i][1] == places[i][2] && places[i][0] != 0)
                return i;
        }
        return -1;
    }
    protected int CheckForMatchCol()
    {
        for(int i=0;i<3;i++)
        {
            if(places[0][i] == places[1][i] && places[1][i] == places[2][i] && places[0][i] != 0)
                return i;
        }
        return -1;
    }
    protected boolean CheckForMatchDiagonal1() {
        if (places[0][0] == places[1][1] && places[1][1] == places[2][2] && places[1][1] != 0) {
            return true;
        } else {
            return false;

        }
    }
    protected boolean CheckForMatchDiagonal2()
    {
        if(places[2][0] == places[1][1] && places[1][1] == places[0][2] && places[1][1] != 0)
            return true;
        else
            return false;
    }
    protected void CheckWin()
    {

        if(CheckForMatchCol() != (-1))
        {

            if(places[0][CheckForMatchCol()] == 1)
            {
                tv.setText("X won in "+amount_of_moves_player1 + " moves");
                wins_of_player1++;
            }
            else
            {
                tv.setText("O won in "+amount_of_moves_player2 + " moves");
                wins_of_player2++;
            }
            has_to_newgame = true;
        }
        else if(CheckForMatchRow() != (-1))
        {
            if(places[CheckForMatchRow()][0] == 1)
            {
                tv.setText("X won in "+amount_of_moves_player1 + " moves");
                wins_of_player1++;
            }
            else
            {
                tv.setText("O won in "+amount_of_moves_player2 + " moves");
                wins_of_player2++;
            }


            has_to_newgame = true;
        }
        else if(CheckForMatchDiagonal1())
        {

            if(places[0][0] == 1)
            {
                tv.setText("X won in "+amount_of_moves_player1 + " moves");
                wins_of_player1++;
            }
            else
            {
                tv.setText("O won in "+amount_of_moves_player2 + " moves");
                wins_of_player2++;
            }
            has_to_newgame = true;
        }
        else if(CheckForMatchDiagonal2())
        {
            if(places[0][2] == 1)
            {
                tv.setText("X won in "+amount_of_moves_player1 + " moves");
                wins_of_player1++;
            }
            else
            {
                tv.setText("O won in "+amount_of_moves_player2 + " moves");
                wins_of_player2++;
            }
            has_to_newgame = true;
        }

        //if(has_to_newgame == true)
        //{
            //Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            //v.vibrate(100);
            //v.vibrate(5000);
        //}
        if(!ExistMaraton())
        {
            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(1000);
            if(wins_of_player1 > wins_of_player2)
                tv.setText("X won 0 in maraton!");
            else
            {
                tv.setText("0 won X in maraton!");
            }
            //v.vibrate(5000);
        }

    }

    protected void Restart()
    {
        for(int i=0;i<3;i++) {

            for (int j = 0; j < 3; j++) {
                places[i][j] = 0;
            }
        }
        for(int i=0;i<3;i++) {

            for (int j = 0; j < 3; j++) {
                btns[i][j].setText("_");
            }
        }
        has_to_newgame = false;
        tv.setText("New game.");
        this.amount_of_current_games--;
        amount_of_moves_player1 = 0;
        amount_of_moves_player2 = 0;
        player_playing = 1;
    }
    protected int CheckSlotsAvailable()
    {
        int count = 0;
        for(int i=0; i < 3; i++)
        {
            for(int j=0;j < 3; j++)
            {
                if(places[i][j] == 0)
                    count++;
            }
        }
        return count;
    }
    protected boolean AvailabePlace(int i,int j) {
        if (places[i][j] != 2 && places[i][j] != 1) {
            return true;

        } else {
            return false;

        }
    }
}
