package com.example.liav.exhanucadec2017_c1;

import android.app.Activity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.view.*;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class MainActivity extends Activity {

    private TextView textview1,textview2;
    private EditText edit1;
    private SeekBar seekBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textview1 = (TextView) findViewById(R.id.textView3);
        edit1 = (EditText) findViewById(R.id.editText);
        textview2 = (TextView) findViewById(R.id.textView4);
        addListenerOnSeekBar();
    }



    public void addListenerOnSeekBar()
    {
        seekBar =(SeekBar) findViewById(R.id.seekBar);
        seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) {
                // TODO Auto-generated method stub

                textview1.setText("%" + progress);
                String text = edit1.getText().toString();
                String text_toast;
                try {
                    int num = Integer.parseInt(text);
                    double n = (((double)progress/100)*(double)num + (double)(num));
                    if(n > 100)
                        n = 100;
                    text_toast = "Grade is: "+ (n);
                }
                catch(NumberFormatException e)
                {
                    text_toast = "Wrong input";
                }
                //textview2.setText(text_toast);
                Toast.makeText(getApplicationContext(), String.valueOf(text_toast),Toast.LENGTH_SHORT).show();

            }
        });
    }


    /*
    public void addListenerOnButton()
    {
        edit1 = (EditText)findViewById(R.id.edit1);
        edit2 = (EditText)findViewById(R.id.edit2);
        buttonSum = (Button)findViewById(R.id.button1);

        buttonSum.setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View v) {
                String value1 = edit1.getText().toString();
                String value2 = edit2.getText().toString();
                int a = Integer.parseInt(value1);
                int b = Integer.parseInt(value2);
                int sum = a+b;
                Toast.makeText(getApplicationContext(),String.valueOf(sum),Toast.LENGTH_LONG).show();
            }


        });

    }
    */
}
