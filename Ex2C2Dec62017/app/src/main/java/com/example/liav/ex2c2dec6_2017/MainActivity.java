package com.example.liav.ex2c2dec6_2017;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.EditText;
import java.util.Random;

public class MainActivity extends Activity {

    private TextView avgTitle;
    private EditText edit1,edit2;
    private Button buttonSum,buttonMul,buttonSub;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addListenerOnButton();
    }

    public void addListenerOnButton()
    {
        edit1 = (EditText)findViewById(R.id.edit1);
        edit2 = (EditText)findViewById(R.id.edit2);
        buttonSum = (Button)findViewById(R.id.buttonSum);
        buttonSub = (Button)findViewById(R.id.buttonSub);
        buttonMul = (Button)findViewById(R.id.buttonMul);
        avgTitle = (TextView) findViewById(R.id.avgCap);
        buttonSum.setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View v) {
                String value1 = edit1.getText().toString();
                String value2 = edit2.getText().toString();
                int a = Integer.parseInt(value1);
                int b = Integer.parseInt(value2);
                int sum = (a+b)/2;
                int mul= a*b;
                int sub = a-b;

                avgTitle.setText("Sum is: "+sum);
            }


        });
        buttonMul.setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View v) {
                String value1 = edit1.getText().toString();
                String value2 = edit2.getText().toString();
                int a = Integer.parseInt(value1);
                int b = Integer.parseInt(value2);
                int sum = (a+b)/2;
                int mul= a*b;
                int sub = a-b;

                avgTitle.setText("Multiply of them: " + mul);
            }


        });
        buttonSub.setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View v) {
                String value1 = edit1.getText().toString();
                String value2 = edit2.getText().toString();
                int a = Integer.parseInt(value1);
                int b = Integer.parseInt(value2);
                int sum = (a+b)/2;
                int mul= a*b;
                int sub = a-b;

                avgTitle.setText("Subtract of them: "+ sub);
            }


        });

    }
}
