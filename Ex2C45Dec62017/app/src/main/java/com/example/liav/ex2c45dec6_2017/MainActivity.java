package com.example.liav.ex2c45dec6_2017;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.EditText;
import java.util.Random;
public class MainActivity extends Activity {

    private TextView title;
    private EditText edit1,edit2;
    private Button button1,button2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addListenerOnButton();
    }

    public void addListenerOnButton()
    {
        edit1 = (EditText)findViewById(R.id.edit1);
        edit2 = (EditText)findViewById(R.id.edit2);
        button1 = (Button)findViewById(R.id.button1);
        button2 = (Button)findViewById(R.id.button2);
        title = (TextView) findViewById(R.id.avgCap);
        button1.setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View v) {
                String value1 = edit1.getText().toString();

                int a = Integer.parseInt(value1);
                int f = (9/5*a+32);

                title.setText(""+f);
            }


        });
        button2.setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View v) {
                String value1 = edit2.getText().toString();

                int a = Integer.parseInt(value1);
                int f = (a*60);

                title.setText(""+f);
            }


        });

    }
}
