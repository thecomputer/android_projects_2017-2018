package com.example.liav.ex1nov24_2017;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import java.util.Random;

public class MainActivity extends Activity {

    private TextView view1,view2,view3;
    private Button buttonSum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        view1 = (TextView) findViewById(R.id.view1);
        view2 = (TextView) findViewById(R.id.view2);
        view3 = (TextView) findViewById(R.id.view3);
        view1.setText("");
        view2.setText("");
        view3.setText("");
        addListenerOnButton();
    }

    public void addListenerOnButton()
    {

        buttonSum = (Button)findViewById(R.id.button1);

        buttonSum.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                view1 = (TextView) findViewById(R.id.view1);
                view2 = (TextView) findViewById(R.id.view2);
                view3 = (TextView) findViewById(R.id.view3);
                Random rand = new Random();
                int a;
                a = rand.nextInt(899);
                a = a + 100;
                int b;
                b = rand.nextInt(899);
                b = b + 100;
                view1.setText(Integer.toString(a));

                if(a > b) {
                    view2.setText(">");
                }
                else
                {
                    if(a < b)
                        view2.setText("<");
                    else
                        view2.setText("=");
                }
                view3.setText(Integer.toString(b));
            }

        });

    }
}
